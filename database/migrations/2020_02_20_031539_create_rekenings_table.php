<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRekeningsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rekenings', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->integer('jurnal_id')->unsigned();
            $table->foreign('jurnal_id')
                ->references('id')->on('jurnals')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->string('nama', 100);
            $table->decimal('saldo', 11, 2)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rekenings');
    }
}
