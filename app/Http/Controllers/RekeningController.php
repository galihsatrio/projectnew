<?php

namespace App\Http\Controllers;

use App\Rekening;
use Illuminate\Http\Request;
use App\Http\Requests\SendRequest;
use Illuminate\Support\Facades\DB;

class RekeningController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if($request->has('search')){
            $search = $request->get('search');

            $rekening = DB::table('rekenings')->join('jurnals','jurnals.id','=','rekenings.jurnal_id')
            ->select('rekenings.*','jurnals.*')->where('nama','like',"%".$search."%")->paginate();

        }else{

            $rekening = DB::table('rekenings')->join('jurnals','jurnals.id','=','rekenings.jurnal_id')
            ->select('rekenings.*','jurnals.*')->paginate(7);
        }

        $jurnal = DB::table('jurnals')->get();


        return view('rekening.index', compact('rekening','jurnal'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    // public function send(SendRequest $request)
    // {

    // }

    public function store(SendRequest $request)
    {
        $validated = $request->validated();

        DB::table('rekenings')->insert([
            'jurnal_id' => $request->jurnal_id,
            'nama' => $request->nama,
            'saldo' => $request->saldo
        ]);

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $rekening = DB::table('rekenings')->where('id', $id)->first();
        $jurnal = DB::table('jurnals')->where('id', $rekening->jurnal_id)->first();

        return view('rekening.edit', compact('rekening','jurnal'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::table('rekenings')->where('id', $id)->update([
            'nama' => $request->nama,
            'saldo' => $request->saldo
        ]);

        return redirect('/rekening');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $rekening = Rekening::find($id);
        $rekening->delete();

        return redirect('/rekening');
    }
}
