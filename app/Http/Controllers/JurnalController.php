<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Http\Requests\SendRequest;
use Illuminate\Support\Facades\DB;
use \App\Jurnal;
use \App\Rekening;

class JurnalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->has('search')){
            $search = $request->get('search');

            $item = [];

            $jurnal = DB::table('jurnals')->where('keterangan','like',"%".$search."%")->paginate();
            foreach($jurnal as $jurnals){
                $rekening = DB::table('rekenings')->where('jurnal_id', $jurnals->id)->get();
                $total = DB::table('rekenings')->where('jurnal_id', $jurnals->id)->sum('saldo');

                $item[$jurnals->id] = [
                    'rekening' => $rekening,
                    'total' => $total
                ];
            }
        }else{

            $item = [];

            $jurnal = DB::table('jurnals')->paginate(5);
            foreach($jurnal as $jurnals){
                $rekening = DB::table('rekenings')->where('jurnal_id', $jurnals->id)->get();
                $total = DB::table('rekenings')->where('jurnal_id', $jurnals->id)->sum('saldo');

                $item[$jurnals->id] = [
                    'rekening' => $rekening,
                    'total' => $total
                ];
            }
        }

        return view('jurnal.index', compact('jurnal','item'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Jurnal::create([
            'wkt_jurnal' => $request->wkt_jurnal,
            'keterangan' => $request->keterangan
        ]);

        return redirect('/jurnal');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $jurnal = DB::table('jurnals')->where('id', $id)->first();

        return view('jurnal.edit', compact('jurnal'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::table('jurnals')->where('id', $id)->update([
            'wkt_jurnal' => $request->wkt_jurnal,
            'keterangan' => $request->keterangan
        ]);

        return redirect('/jurnal');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $jurnal = Jurnal::find($id);
        $jurnal->delete();

        return redirect('jurnal');
    }

    public function storeItem(SendRequest $request)
    {
        $error = $request->all();

        DB::table('rekenings')->insert($error, [
            'jurnal_id' => $request->jurnal_id,
            'nama' => $request->nama,
            'saldo' => $request->saldo
        ]);

        return redirect('/jurnal');
    }

    public function editItem($id)
    {
        $rekening = DB::table('rekenings')->where('id', $id)->first();
        $jurnal = DB::table('jurnals')->where('id', $rekening->jurnal_id)->get();

        return view('jurnal.item.edit', compact('jurnal','rekening'));
    }

    public function updateItem(SendRequest $request, $id)
    {
        DB::table('rekenings')->where('id', $id)->update([
            'nama' => $request->nama,
            'saldo' => $request->saldo
        ]);

        return redirect('/jurnal');
    }

    public function destroyItem($id)
    {
        $item = Rekening::find($id);
        $item->delete();

        return redirect('/jurnal');
    }
}
