<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
Use \App\User;


class AuthController extends Controller
{
    public function index()
    {
        $user = DB::table('Users')->get();
        return view('auth.index', compact('user'));
    }


    public function login()
    {
        return view('auth.login');
    }

    public function postLogin(Request $request)
    {
        $auth = Auth::attempt([
            'email' => $request->email,
            'password' => $request->password
        ]);

        if($auth){
            return redirect('/home');
        }else{
            return redirect('/login')->with('gagal', 'Email atau password yang anda masukkan salah');
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/login');
    }

    public function register()
    {
        return view('auth.register');
    }

    public function postRegister(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:3',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6|confirmed'
        ]);

        User::create([
            'role' => $request->role,
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password)
        ]);

        return redirect('register')->with('berhasil','Selamat akun anda berhasil di daftarkan');
    }
}
