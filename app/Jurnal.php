<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jurnal extends Model
{
    protected $fillable = ['wkt_jurnal','keterangan'];

    public function rekenings()
    {
        return $this->hasMany(Rekening::class);
    }

    protected $date = [ 'wkt_jurnal', 'created_at', 'update_at' ];
}
