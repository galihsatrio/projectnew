<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rekening extends Model
{
    protected $fillable = ['jurnal_id','nama','saldo'];

    public function Jurnals()
    {
        return $this->hasOne(Jurnal::class);
    }
}
