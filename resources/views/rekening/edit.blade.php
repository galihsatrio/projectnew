@extends('layout.master')

@section('title','Update Rekening')

@section('container')

<div class="row">
    <div class="col-md-12">
        <div class="panel">


            <div class="panel-heading">
                <h3> Tambah Rekening</h3>
            </div>

            <div class="panel-body">
                <div class="col-md-5">
                    <form method="POST" action="/rekening/{{$rekening->id}}">
                        @csrf
                        <div class="form-group">
                            <label for="Jurnal_id">Keterangan</label>
                            <select name="jurnal_id" id="Jurnal_id" class="form-control" disabled>
                                <option value="{{$jurnal->id}}"> {{$jurnal->id}}. {{$jurnal->keterangan}} </option>
                            </select>
                        </div>
                        <div class="form-group">
                          <label for="nama">Nama</label>
                          <input type="text" class="form-control" id="nama" placeholder="Masukkan Nama" name="nama" value="{{$rekening->nama}}">
                        </div>
                        <div class="form-group">
                            <label for="saldo">Saldo</label>
                            <input type="text" class="form-control" id="saldo" placeholder="Masukkan saldo" name="saldo" value="{{ floor($rekening->saldo)}}">
                        </div>
                        <br>

                        <button type="submit" class="btn btn-success btn-sm">update</button>
                        <a href="/rekening" class="btn btn-default btn-sm">kembali</a>

                        <br><br><br><br><br><br><br>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>

@endsection
