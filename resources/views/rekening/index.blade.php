@extends('layout.master')


@section('title','Modul Rekening')

@section('search')
<form class="navbar-form navbar-left" method="get" action="/rekening">
    <div class="input-group">
        <input type="text" value="" class="form-control" placeholder="Search..." name="search">
        <span class="input-group-btn"><button type="button" class="btn btn-primary">Go</button></span>
    </div>
    <a href="/rekening" class="btn btn-default btn-sm">ShowAll</a>
</form>
@endsection

@section('container')
<div class="row">
    <div class="col-md-12">
        <div class="panel">

            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif

            <div class="panel-heading">
                <h2>Rekening</h2>
            </div>


            <div class="panel-body">

                <div class="right">
                    <button type="button" class="btn btn-primary btn-sm float-right" data-toggle="modal" data-target="#tambahRekening">
                        <i class="fa fa-plus-square"></i>
                    </button>
                </div>

                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Keterangan</th>
                            <th>Nama</th>
                            <th>Saldo</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($rekening as $no => $rekenings)
                        <tr>
                            <td>{{$no + $rekening->firstitem()}}</td>
                            <td>{{$rekenings->keterangan}}</td>
                            <td>{{$rekenings->nama}}</td>
                            <td>{{number_format($rekenings->saldo, 0,',','.')}}</td>
                            <td width="250px">
                                <a href="/rekening/{{$rekenings->id}}/edit" class="btn btn-success btn-sm prepend">
                                    <i class="fa fa-refresh"></i>
                                </a>
                                <a href="/rekening/{{$rekenings->id}}/delete" onclick="return confirm('Apakah Anda ingin menghapusnya ?')" class="btn btn-danger btn-sm">
                                    <i class="fa fa-trash-o"></i>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

                <div class="container">
                    {{ $rekening->links() }}
                </div>

            </div>
        </div>
    </div>
</div>

  <!-- Modal -->
<div class="panel-modal">
    <div class="modal fade" id="tambahRekening" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h3 class="modal-title" id="exampleModalLabel">Tambah Rekening</h3>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <form method="post" action="/rekening/store">
                    @csrf
                    <div class="form-group">
                        <label for="keterangan">Keterangan</label>
                        <select class="form-control" name="jurnal_id">
                            <option>- Pilih -</option>
                            @foreach($jurnal as $jurnals)
                            <option value="{{$jurnals->id}}">{{$jurnals->id}}. {{$jurnals->keterangan}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                      <label for="nama">Nama</label>
                      <input type="text" class="form-control" id="nama" placeholder="Masukkan nama" name="nama">
                    </div>
                    <div class="form-group">
                        <label for="saldo">saldo</label>
                        <input type="text" class="form-control" id="saldo" placeholder="Masukkan saldo" name="saldo">
                    </div>

                    <button type="submit" class="btn btn-primary btn-sm">Tambah</button>
                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Kembali</button>
                </form>
            </div>
          </div>
        </div>
      </div>
</div>
@endsection
