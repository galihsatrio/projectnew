@extends('layout.auth')

@section('title','Daftar')

@section('container')

@if(Session::has('berhasil'))
    <div class="alert alert-success">
        <strong>{{ Session::get('berhasil') }}</strong>
    </div>
    <button type="button" class="close" data-dismiss="alert" aria-label="close">
        <span aria-hidden="true">&times;</span>
    </button>
@endif

<div class="panel">



    <div class="panel-heading mb-4">
        <h2>Register</h2>
    </div>
    <div class="panel-body">
        <form action="/register/post" method="post">
            @csrf

            <div class="form-group">
                <select class="form-control" id="role" name="role">
                  <option>User</option>
                  <option>Admin</option>
                </select>
            </div>

            <div class="form-group">
                <input type="text" class="form-control {{ $errors->has('name') ? 'is-invalid' : ''}}" placeholder="Nama Lengkap" name="name" value="{{old('name')}}" required>
                @if ($errors->has('name'))
                    <div class="invalid-feedback">
                        {{$errors->first('name')}}
                    </div>
                @endif
            </div>
            <div class="form-group">
                <input type="email" class="form-control mt-2 {{ $errors->has('email') ? 'is-invalid' : ''}}" placeholder="Email" name="email" value="{{old('email')}}" required>
                @if ($errors->has('email'))
                    <div class="invalid-feedback">
                        {{ $errors->first('email') }}
                    </div>
                @endif
            </div>
            <div class="form-group">
                <input type="password" class="form-control mt-2 {{ $errors->has('password') ? 'is-invalid' : ''}}" placeholder="Password" name="password" required>
                @if ($errors->has('password'))
                    <div class="invalid-feedback">
                        {{$errors->first('password')}}
                    </div>
                @endif
            </div>
            <div class="form-group">
                <input type="password" class="form-control mt-2 {{ $errors->has('password_confirmation') ? 'is-invalid' : ''}}" placeholder="Password Confirmation" name="password_confirmation" required>
                @if ($errors->has('password_confirmation'))
                    <div class="invalid-feedback">
                        {{$errors->first('password_confirmation')}}
                    </div>
                @endif
            </div>


            <button class="btn btn-primary btn-block mt-2">Daftar</button>
            <p class="mt-2">Sudah punya akun? <a href="/login">Masuk</a> </p>
        </form>
    </div>
</div>
@endsection
