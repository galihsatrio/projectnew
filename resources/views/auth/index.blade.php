@extends('layout.master')

@section('title','User')

@section('search')

@endsection


@section('container')
<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel-heading">
                <h3>User</h3>
            </div>

            <div class="panel-body">
                <table class="table table-hover">
                    <thead class="thead">
                        <tr>
                            <th>No</th>
                            <th>Role</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($user as $users)
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>{{$users->role}}</td>
                            <td>{{$users->name}}</td>
                            <td>{{$users->email}}</td>
                            <td>
                                <a href="" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

        </div>

    </div>
</div>
@endsection
