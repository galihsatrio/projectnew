@extends('layout.auth')

@section('title','Masuk')

@section('container')

@if (Session::has('gagal'))
<div class="alert alert-warning">
    {{Session::get('gagal')}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

    <div class="panel">
        <div class="panel-heading mb-5">
            <h2>Login</h2>
        </div>

        <div class="panel-body">
            <form action="/login/post" method="POST">
                @csrf
                <input type="email" class="form-control my-2" placeholder="Email" name="email" required>
                <input type="password" class="form-control mt-2" placeholder="Password" name="password" required>

                <button class="btn btn-primary btn-block mt-2">Login</button>
                <p class="mt-2">Belum punya akun? <a href="/register">Daftar</a></p>
            </form>
        </div>

    </div>
@endsection
