@extends('layout.master')

@section('title','update Item')

@section('container')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel">

                <div class="panel-heading">
                    <h2>Update Item</h2>
                </div>

                <div class="panel-body">
                    <div class="col-md-6">
                        <form action="/jurnal/item/{{$rekening->id}}" method="post">
                            @csrf
                            <div class="form-group">
                                <label for="Jurnal_id">Keterangan</label>
                                @foreach($jurnal as $jurnals)
                                <select name="jurnal_id" id="Jurnal_id" class="form-control" value="{{$jurnals->id}}" disabled>
                                    <option value="{{$jurnals->id}}"> {{$jurnals->id}}. {{$jurnals->keterangan}} </option>
                                </select>
                                @endforeach
                            </div>
                            <div class="form-group">
                              <label for="nama">Nama</label>
                              <input type="text" class="form-control" id="nama" placeholder="Masukkan Nama" name="nama" value="{{$rekening->nama}}">
                            </div>
                            <div class="form-group">
                                <label for="saldo">saldo</label>
                                <input type="text" class="form-control" id="saldo" placeholder="Masukkan saldo" name="saldo" value="{{ floor($rekening->saldo)}}">
                            </div>

                            <button class="btn btn-success btn-xs">update</button>
                            <a href="/jurnal" class="btn btn-warning btn-xs">kembali</a>
                        </form>

                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection
