@extends('layout.master')

@section('title','Jurnal')

@section('search')
<form class="navbar-form navbar-left" method="get" action="/jurnal">
    <div class="input-group">
        <input type="text" value="" class="form-control" placeholder="Cari keterangan..." name="search">
        <span class="input-group-btn"><button type="button" class="btn btn-primary">Go</button></span>
    </div>
    <a href="/jurnal" class="btn btn-default btn-sm">ShowAll</a>
</form>
@endsection

@section('container')
<div class="row">
    <div class="col-md-12">
        <div class="panel">

            <div class="panel-heading">
                <h2>Jurnal</h2>
                <div class="right">
                    <button type="button" class="btn" data-toggle="modal" data-target="#modalJurnal">
                        <i class="lnr lnr-plus-circle"></i>
                    </button>
                </div>
            </div>


            <div class="panel-body">
                @if($errors->any())
                <div class="alert-danger">
                    @foreach($errors->all() as $error)
                        <li><span>{{$error}}</span></li>
                    @endforeach
                </div>
                @endif

                @if(count($jurnal))
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Waktu</th>
                            <th>Keterangan</th>
                            <th class="text-center">Item</th>
                            <th width="240px" class="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($jurnal as $no => $jurnals)
                        <tr>
                            <td>{{ $no+$jurnal->firstItem() }}</td>
                            <td>
                                {{ \Carbon\Carbon::parse($jurnals->wkt_jurnal)->format('d, M Y') }} <br> <br> <br>
                                {{ \Carbon\Carbon::parse($jurnals->created_at)->diffForHumans() }}

                            </td>
                            <td>{{$jurnals->keterangan}}</td>
                            <td>
                                @if(count($item[$jurnals->id]['rekening']))
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama</th>
                                            <th>Saldo</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($item[$jurnals->id]['rekening'] as $rekening)
                                        <tr>
                                            <td>{{$loop->iteration}}</td>
                                            <td>{{$rekening->nama}}</td>
                                            <td>{{ number_format($rekening->saldo, 0, ',','.') }}</td>
                                            <td width="250px">
                                                <a href="/jurnal/item/{{$rekening->id}}/edit" class="btn btn-success btn-xs">
                                                    <i class="fa fa-refresh"></i>
                                                </a>

                                                <a href="/jurnal/item/{{$rekening->id}}/delete" class="btn btn-danger btn-xs" onclick="return confirm('Apakah Anda yakin menghapus item ini ?')">
                                                    <i class="fa fa-trash-o"></i>
                                                </a>

                                            </td>
                                        </tr>
                                        @endforeach
                                        <td colspan="4">Total Saldo : {{ number_format($item[$jurnals->id]['total'], 0, ',','.') }} </td>
                                    </tbody>
                                </table>
                                @else
                                    <p class="text-center"> "Item Kosong"</p>
                                    <p class="text-center">Klik "Add Item" untuk menambahkan item</p>
                                @endif
                            </td>
                            <td class="text-center">
                                <button type="button" class="btn btn-primary btn-xs btn-my-1" data-toggle="modal" data-target="#modalItem">
                                    <i class="fa fa-plus-square"></i>
                                </button>
                                <a href="/jurnal/{{$jurnals->id}}/edit" class="btn btn-success btn-xs">
                                    <i class="fa fa-refresh"></i>
                                </a>
                                <a href="/jurnal/{{$jurnals->id}}/delete" class="btn btn-danger btn-xs" onclick="return confirm('Apakah Anda ingin menghapusnya ?')">
                                    <i class="fa fa-trash-o"></i>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                @else
                    <h4 class="mt-4 text-center">Jurnal Kosong klik "Add" untuk menambahkan jurnal</h4>
                @endif

                <div class="container">
                    {{ $jurnal->links() }}
                </div>
            </div>

        </div>
    </div>
</div>
@endsection


  <!-- Modal Jurnal -->
  <div class="modal fade" id="modalJurnal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLabel">Tambah Jurnal</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form method="POST" action="/jurnal/tambah">
                @csrf
                <div class="form-group">
                  <label for="waktu">Waktu</label>
                  <input type="date" class="form-control" id="waktu" placeholder="masukkan waktu" name="wkt_jurnal" required>
                </div>
                <div class="form-group">
                    <label for="keterangan">keterangan</label>
                    <input type="text" class="form-control" id="keterangan" placeholder="masukkan keterangan" name="keterangan" required>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-xs">Tambah</button>
                    <button type="button" class="btn btn-warning btn-xs" data-dismiss="modal">Kembali</button>
                </div>
            </form>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal Item -->
  <div class="modal fade" id="modalItem" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLabel">Tambah Item</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form method="POST" action="/jurnal/item/tambah">
                @csrf
                <div class="form-group">
                    <label for="Jurnal_id">Keterangan</label>
                    <select name="jurnal_id" id="Jurnal_id" class="form-control">
                        @foreach($jurnal as $jurnals)
                        <option value="{{$jurnals->id}}"> {{$jurnals->id}}. {{$jurnals->keterangan}} </option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                  <label for="nama">Nama</label>
                  <input type="text" class="form-control" id="nama" placeholder="Masukkan Nama" name="nama" required>
                </div>
                <div class="form-group">
                    <label for="saldo">Saldo</label>
                    <input type="text" class="form-control" id="saldo" placeholder="Masukkan Saldo" name="saldo" required>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-xs">Tambah</button>
                    <button type="button" class="btn btn-warning btn-xs" data-dismiss="modal">Kembali</button>
                </div>
            </form>
        </div>
      </div>
    </div>
  </div>
