@extends('layout.master')

@section('title','Edit Jurnal')

@section('container')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-heading">
                    <h2>Update Jurnal</h2>
                </div>

                <div class="panel-body">
                    <div class="col-md-8">
                        <form method="post" action="/jurnal/{{$jurnal->id}}">
                            @csrf
                            <div class="form-group">
                              <label for="waktu">Waktu</label>
                              <input type="date" class="form-control" id="waktu" placeholder="Masukkan Waktu" name="wkt_jurnal" value="{{$jurnal->wkt_jurnal}}" required>
                            </div>
                            <div class="form-group">
                                <label for="keterangan">keterangan</label>
                                <input type="text" class="form-control" id="keterangan" placeholder="Masukkan keterangan" name="keterangan" value="{{$jurnal->keterangan}}" required>
                            </div>
                            <br><br>

                           <button type="submit" class="btn btn-success btn-xs">update</button>
                           <a href="/jurnal" class="btn btn-warning btn-xs">kembali</a>

                           <br><br><br><br><br><br><br>
                        </form>
                    </div>
                </div>


            </div>
        </div>
    </div>
</div>

@endsection
