<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login','AuthController@login')->name('login');
Route::post('/login/post', 'AuthController@postLogin');
Route::get('/logout','AuthController@logout');
Route::get('/register','AuthController@register');
Route::post('/register/post', 'AuthController@postRegister');




Route::get('/home','HomeController@index');


Route::group(['middleware'=> ['auth', 'CheckRole:admin']], function(){

    Route::get('/user', 'AuthController@index');

});

Route::group(['middleware' => ['auth', 'CheckRole:admin,User']], function(){

    Route::get('/rekening','RekeningController@index');
    Route::post('/rekening/store','RekeningController@store');
    Route::get('/rekening/{id}/edit','RekeningController@edit');
    Route::post('/rekening/{id}','RekeningController@update');
    Route::get('/rekening/{id}/delete','RekeningController@destroy');

    Route::get('/jurnal','JurnalController@index');
    Route::post('/jurnal/tambah', 'JurnalController@store');
    Route::get('/jurnal/{id}/edit','JurnalController@edit');
    Route::post('/jurnal/{id}', 'JurnalController@update');
    Route::get('/jurnal/{id}/delete','JurnalController@destroy');

    //item
    Route::post('/jurnal/item/tambah','JurnalController@storeItem');
    Route::get('/jurnal/item/{id}/edit','JurnalController@editItem');
    Route::post('/jurnal/item/{id}','JurnalController@updateItem');
    Route::get('/jurnal/item/{id}/delete','JurnalController@destroyItem');
});
